/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import 'react-native-gesture-handler';
import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {View, Text, StyleSheet, Button, FlatList} from 'react-native';
import { TouchableHighlight, TouchableOpacity } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/FontAwesome5';
import SQLite from 'react-native-sqlite-storage';

var db = SQLite.openDatabase({
  name:'TestDB.db',
})
class BuatDb extends React.Component{
  state={
    user: [],
  };
  constructor(props){
    super(props);
    db.transaction(function(txn) {
      txn.executeSql(
        "SELECT name FROM sqlite_sequence WHERE name='table' AND seq='table_user'",
        [],
        function(tx, res) {
          console.log('item:', res.rows.length);
          if (res.rows.length == 0) {
            txn.executeSql('DROP TABLE IF EXISTS test', []);
            txn.executeSql(
              'CREATE TABLE IF NOT EXISTS table_user(user_id INTEGER PRIMARY KEY AUTOINCREMENT, CREATE TABLE "test" ( "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE, "name" TEXT NOT NULL, "age" NUMERIC NOT NULL, "email" TEXT NOT NULL )user_name VARCHAR(20), user_contact INT(10), user_address VARCHAR(255))',
              []
            );
          }
        }
      );
    });
    console.log('aaa');
    db.transaction(function(tx) {
      tx.executeSql(
        'insert into test values ("aaa",20,"aaa@email.com"),("bbb",25,"bbb@email.com"),("ccc",40,"ccc@email.com")',[],
        (tx, results) => {
          console.log('Results', results.rowsAffected);
          if (results.rowsAffected > 0) {
            Alert.alert(
              'Success',
              'You are Registered Successfully',
              [
                {
                  text: 'Ok',
                  onPress: () =>
                    that.props.navigation.navigate('home'),
                },
              ],
              { cancelable: false }
            );
          } else {
            alert('Registration Failed');
          }
        }
      );
    });
  }
  ListViewItemSeparator = () => {
    return (
      <View style={{ height: 0.2, width: '100%', backgroundColor: '#000' }} />
    );
  };
  render() {
    return (
      <View style={styles.content}>
        <Text>Id</Text>
        <FlatList
          data={this.state.user}
          ItemSeparatorComponent={this.ListViewItemSeparator}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({ item }) => (
            <View key={item.id} style={{ backgroundColor: '#000', padding: 20 }}>
              <Text>Id: {item.id}</Text>
              <Text>Name: {item.name}</Text>
              <Text>Age: {item.age}</Text>
              <Text>Email: {item.email}</Text>
            </View>
          )}
        />
        
      </View>
    );
  }
}

class User extends React.Component{

}

function HomeScreen({navigation}){
  return (
    <View style={styles.content}>
      <Text>Home Screen</Text>
      <Button
        title='Ayo Latihan!'
        onPress={()=>navigation.navigate('latihan')}/>
      <Button
      title='Lihat list user'
      onPress={()=>navigation.navigate('buatdb')}/>
    </View>
  );
}

function Latihan({navigation}){
  return(
    <View style={styles.content}>
      <Text>Latihan Soal</Text>
      <Button
        title='Mulai'
        onPress={()=>navigation.navigate('soal1')}/>
    </View>
  )
}

function Soal1({navigation}){
  return(
    <View style={{backgroundColor: '#ecf0f1'}}>
      <Text style={styles.poin}>Poin kamu : 0</Text>
      <Text style={styles.soal}>5+3</Text>
      <TouchableHighlight
        style={styles.jawaban}
        onPress={() => {
          navigation.navigate('penjelasan1',{
            score: 10,
            hasil: 'BENAR',
            banner: 'SELAMAT!!',
          }),
          correct.play()
        }}
        underlayColor= '#2ecc71'>
        <Text style={styles.pilihan}>A. 8</Text>
      </TouchableHighlight>
      <TouchableHighlight
        style={styles.jawaban}
        onPress={() => {
          navigation.navigate('penjelasan1',{
            score: 0,
            hasil: 'SALAH',
            banner: 'YAH,',
          }),
          wrong.play()
        }}
        underlayColor= '#e74c3c'>
        <Text style={styles.pilihan}>B. 10</Text>
      </TouchableHighlight>
      <TouchableHighlight
        style={styles.jawaban}
        onPress={() => {
          navigation.navigate('penjelasan1',{
            score: 0,
            hasil: 'SALAH',
            banner: 'YAH,',
          }),
          wrong.play()
        }}
        underlayColor= '#e74c3c'>
        <Text style={styles.pilihan}>C. 2</Text>
      </TouchableHighlight>
    </View>
  )
}

function PenjelasanSoal1({route,navigation}){
  const { score } = route.params;
  const { hasil } = route.params;
  const { banner } = route.params;
  const {totalScore} = route.params;
  return(
    <>
    <View style={{backgroundColor: '#ecf0f1'}}>
      <Text style={styles.banner}>{JSON.parse(JSON.stringify(banner))} JAWABAN KAMU {JSON.parse(JSON.stringify(hasil))}</Text>
      <Text style={styles.penjelasan}>5+3 hasilnya adalah 8</Text>
      <Text style={styles.poin}>Poin kamu: +{JSON.stringify(score)}</Text>
      <Text style={styles.poin}>Total poin kamu: {JSON.stringify(score)}</Text>
    </View>
    <View style={styles.fixToText}>
      <TouchableHighlight
        style={styles.backlanjut}
        onPress={() => {
          navigation.navigate('soal1',{
            totalScore: 0
          })
        }}>
        
        <Text style={styles.tulisan}>
          <Icon
            name="arrow-left"
            size={15}
            color='#fff'
          /> Kembali
        </Text>
      </TouchableHighlight>
      <TouchableHighlight
        style={styles.backlanjut}
        onPress={() => {
          navigation.navigate('soal2',{
            totalScore: totalScore + score
          })
        }}>
        <Text style={styles.tulisan}>
          Lanjutkan! <Icon
          name="arrow-right"
          size={15}
          color='#fff'
        /></Text>
      </TouchableHighlight>
    </View>
    </>
  )
}

function Soal2({route,navigation}){
  const { totalScore } = route.params;
  return(
    <View style={{backgroundColor: '#ecf0f1'}}>
      <Text style={styles.poin}>Poin kamu : {JSON.stringify(totalScore)}</Text>
      <Text style={styles.soal}>10*5</Text>
      <TouchableHighlight
        style={styles.jawaban}
        onPress={() => {
          navigation.navigate('penjelasan2',{
            score: 0,
            hasil: 'SALAH',
            banner: 'YAH,',
            totalScore: totalScore,
          }),
          wrong.play()
        }}
        underlayColor= '#e74c3c'>
        <Text style={styles.pilihan}>A. 15</Text>
      </TouchableHighlight>
      <TouchableHighlight
        style={styles.jawaban}
        onPress={() => {
          navigation.navigate('penjelasan2',{
            score: 10,
            hasil: 'BENAR',
            banner: 'SELAMAT!!',
            totalScore: totalScore+10,
          }),
          correct.play()
        }}
        underlayColor= '#2ecc71'>
        <Text style={styles.pilihan}>B. 50</Text>
      </TouchableHighlight>
      <TouchableHighlight
        style={styles.jawaban}
        onPress={() => {
          navigation.navigate('penjelasan2',{
            score: 0,
            hasil: 'SALAH',
            banner: 'YAH,',
            totalScore: totalScore
          }),
          wrong.play()
        }}
        underlayColor= '#e74c3c'>
        <Text style={styles.pilihan}>C. 2</Text>
      </TouchableHighlight>
    </View>
  )
}

function PenjelasanSoal2({route,navigation}){
  const { score } = route.params;
  const { hasil } = route.params;
  const { banner } = route.params;
  const {totalScore} = route.params;
  return(
    <>
    <View style={{backgroundColor: '#ecf0f1'}}>
      <Text style={styles.banner}>{JSON.parse(JSON.stringify(banner))} JAWABAN KAMU {JSON.parse(JSON.stringify(hasil))}</Text>
      <Text style={styles.penjelasan}>10*5 hasilnya adalah 50</Text>
      <Text style={styles.poin}>Poin kamu: +{JSON.stringify(score)}</Text>
      <Text style={styles.poin}>Total poin kamu: {JSON.stringify(totalScore)}</Text>
    </View>
    <View style={styles.fixToText}>
      <TouchableOpacity
        style={styles.backlanjut}
        onPress={() => {
          navigation.navigate('soal2',{
            totalScore: 0
          })
        }}>
        
        <Text style={styles.tulisan}>
          <Icon
            name="arrow-left"
            size={15}
            color='#fff'
          /> Kembali
        </Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.backlanjut}
        onPress={() => {
          navigation.navigate('home',{
            totalScore: totalScore + score
          })
        }}>
        <Text style={styles.tulisan}>
          Selesai <Icon
          name="arrow-right"
          size={15}
          color='#fff'
        /></Text>
      </TouchableOpacity>
    </View>
    </>
  )
}

const Stack = createStackNavigator();

var Sound = require('react-native-sound');

var correct = new Sound('correct.mp3', Sound.MAIN_BUNDLE, (error) => {
  if (error) {
    console.log('failed to load the sound', error);
    return;
  }
});

var wrong = new Sound('wrong.mp3', Sound.MAIN_BUNDLE, (error) => {
  if(error) {
    console.log('failed to load the sound', error);
    return;
  }
})

function App(){
  return(
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="home" component={HomeScreen} options={{title: 'Aplikasi Bebras'}}/>
        <Stack.Screen name="latihan" component={Latihan} options={{title: 'Latihan Soal'}}/>
        <Stack.Screen name="soal1" component={Soal1} options={{title: 'Nomor 1'}}/>
        <Stack.Screen name="penjelasan1" component={PenjelasanSoal1} options={{title: 'Penjelasan'}} initialParams={{totalScore: 0}}/>
        <Stack.Screen name="soal2" component={Soal2} options={{title: 'Nomor 2'}}/>
        <Stack.Screen name="penjelasan2" component={PenjelasanSoal2} options={{title: 'Penjelasan'}}/>
        <Stack.Screen name="buatdb" component={BuatDb} options={{title: 'Buat Database'}}/>
      </Stack.Navigator>
    </NavigationContainer>
  )
}

const styles = StyleSheet.create({
  content: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#ecf0f1'
  },
  soal: {
    textAlign: 'center',
    padding: 20
  },
  jawaban: {
    borderRadius: 10,
    backgroundColor: '#95a5a6',
    paddingVertical: 15,
    marginBottom: 10
  },
  benar: {
    borderRadius: 10,
    backgroundColor: '#2ecc71',
    paddingVertical: 15,
    marginBottom: 10
  },
  salah: {
    borderRadius: 10,
    backgroundColor: '#e74c3c',
    paddingVertical: 15,
    marginBottom: 10
  },
  pilihan: {
    color: '#FFF',
    fontWeight: '600',
    fontSize: 20,
    paddingHorizontal: 50
  },
  banner: {
    textAlign: 'center',
    color: '#27ae60',
    fontSize: 50
  },
  penjelasan: {
    textAlign: 'left',
    fontSize: 20,
    paddingHorizontal: 20,
    fontWeight: '600'
  },
  poin: {
    textAlign: 'left',
    fontSize: 35,
    paddingHorizontal: 20,
    color: '#e74c3c'
  },
  fixToText: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 15,
    position: 'absolute',
    bottom: 0,
    width: '100%'
  },
  backlanjut: {
    marginTop: 30,
    paddingHorizontal: 10,
    paddingVertical: 20,
    backgroundColor: '#d35400',
    borderRadius: 10
  },
  tulisan: {
    color: '#fff'
  }
});

export default App;
