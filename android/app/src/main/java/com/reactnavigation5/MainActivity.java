package com.reactnavigation5;

import com.facebook.react.ReactActivity;
import com.facebook.react.ReactActivityDelegate;
import com.facebook.react.ReactRootView;
import com.swmansion.gesturehandler.react.RNGestureHandlerEnabledRootView;
import org.pgsqlite.SQLitePluginPackage;

public class MainActivity extends ReactActivity {

  // private ReactRootView mReactRootView;

  // @Override
  //   protected void onCreate(Bundle savedInstanceState) {
  //       super.onCreate(savedInstanceState);
  //       mReactRootView = new ReactRootView(this);
  //       mReactRootView.startReactApplication(mReactInstanceManager, "BebrasApp", null); //change "AwesomeProject" to name of your app 
  //       setContentView(mReactRootView);
  //   }

  /**
   * Returns the name of the main component registered from JavaScript. This is used to schedule
   * rendering of the component.
   */
  @Override
  protected String getMainComponentName() {
    return "ReactNavigation5";
  }

  @Override
  protected ReactActivityDelegate createReactActivityDelegate() {
    return new ReactActivityDelegate(this, getMainComponentName()) {
      @Override
      protected ReactRootView createRootView() {
        return new RNGestureHandlerEnabledRootView(MainActivity.this);
      }
    };
  }
}